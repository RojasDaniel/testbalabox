<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contenedores en Árbol</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/estilos.css" media="screen" />

</head>
<body ><br>
<div class="card col-8 center">
  <div class="card-header row">
    <h5 class="col">Bienvenido al primer proyecto, a continuación, tenemos el sistema de manejo para la base de datos
            si desea continuar al proyecto presione el siguiente botón</h5>
    <div class="col-2">
      <button type="button" class="btn btn-primary " onclick=" location.href='inicio.php' ">Iniciar</button>
      <button type="button" class="btn btn-secondary " onclick=" location.href='../index.php' ">Menu</button>
    </div>
  </div>
  <div class="card-body">
    <div class="card-title row">
      <h1 class="col-10 text-center">CRUD:</h1>
      <button type="button" class="btn btn-secondary btn-lg col-2"  onclick=" location.href='formularioInsertar.php' ">Agregar</button>

    </div> 
    <div class="row">
      <div class="col-1">  </div>

        <div class="col">  
          <?php
              include 'conexion.php';
              $sql="select * from contenedor";
              $resultado = $objetoMysqli->query($sql);
          ?> 

            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">id</th>
                  <th scope="col">idPadre</th>
                  <th scope="col">Nombre</th>
                  <th scope="col">Acciones</th>
                </tr>
              </thead>
              <?php
                    while ($filas = $resultado->fetch_assoc()){
                  ?>
              <tbody>
                <tr>
                  <th scope="row"><?php  echo $filas['id']; ?></th>
                  <td><?php echo $filas['idPadre'] ?></td>
                  <td><?php echo $filas['nombre'] ?></td>
                  <td>
                      <a href="eliminar.php?id=<?php echo $filas['id'] ?>">Eliminar</a>
                      <a href="formularioEditar.php?id=<?php echo $filas['id'] ?>&padre=<?php echo $filas['idPadre'] ?>&nombre=<?php echo $filas['nombre'] ?>">Editar</a>
                    </td>

                </tr>
                
              </tbody>
              <?php
                    }
                    include 'cerrarConexion.php';

                ?>
            </table>
        </div>
      <div class="col-1">  </div>
    </div>
  </div>
  <div class="card-footer text-muted text-center">
          Desarrollado por Carlos Rojas
    </div>
  </div>
  <br>
</body>
</html>