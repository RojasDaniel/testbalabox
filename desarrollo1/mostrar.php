<?php  
    $id=$_GET['id'];
    include 'funciones.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>mostrar lista</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/estilos.css" media="screen" />

</head>
<body><br>
<div class="card  col-8">
  <div class="card-header text-center">
    <h1>Has elegido el contenedor <?php echo desarrollo1\funciones::consultarNombre($id); ?></h1>
  </div>
  <div class="card-body">
    <h5 class="card-title">El contenedor raíz de su árbol de dependencias es <?php  desarrollo1\funciones::consultarAbuelo($id); ?>
    </h5>
    <p class="card-text">Árbol de dependencias </p>

    <div class="arbol">
        <ul > 
            <li > <?php echo  desarrollo1\funciones::consultarNombre($id);?></li>
            <li > <?php  desarrollo1\funciones::consultarHijos($id);?></li>
        </ul>
    </div>
  </div>
  <a href="inicio.php" class="btn btn-primary center">Regresar a ver lista de contenedores</a>
<br>
  <div class="card-footer text-muted text-center">
  <p > Desarrollado por Carlos Rojas</p>
  </div>
</div>

    
</body>
</html>
