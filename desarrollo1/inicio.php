<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Desarrollo1</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen" />

</head>
<body class="container">
    <?php
        include 'conexion.php';
        $sql="select id,nombre from contenedor ORDER BY nombre";
        $resultado = $objetoMysqli->query($sql);
    ?> 
<br>
<div class="card text-center">
    <div class="card-header ">
        <div class="row">
            <p class="col-10"> A continuación, se muestran todos los contenedores</p>
            <a href="index.php" class="btn btn-success col-2">Volver al CRUD</a>
        </div>
    </div>
  <div class="card-body">
    <h5 class="card-title">Lista de contenedores</h5>
    <div class="row card-text">
        <div class="col"></div>
        <div class="list-group col center">
            <?php
            while ($filas = $resultado->fetch_assoc()){
            ?>
              <button type="button" class="list-group-item list-group-item-action"aria-current="true" onclick=" location.href='mostrar.php?id=<?php  echo $filas['id']; ?>' ">
              <?php echo $filas['nombre'] ?></button>  
        
        <?php
        }
        ?>
        </div>
        <div class="col"></div>

    </div>
    
  </div>
  <div class="card-footer text-muted">
        Desarrollado por Carlos Rojas
  </div>
</div>

    </div>
    <div class="col">  </div>

</body>
</html>