<?php
$id = $_GET['id'];
$padre = $_GET['padre'];
$nombre = $_GET['nombre'];
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen" />

</head>
<body class="container">
<div class="row">
<div class="col-3">  </div>

    <div class="col">  <br>
<form action="editar.php" method="POST" class="card"> 
  <div class="card-header text-center ">
    <div class="row">
      <p class="col-8"> Edita Contenedor</p>
      <div class="col">  <a href="index.php" class="btn btn-success ">Volver al CRUD</a>
    </div>
  </div>

  </div>
  <div class="card-body">
    <div class="form-group ">
      <label for="id">Id</label>
      <input type="number" class="form-control" id="id" name="id" value="<?php echo $id ?>" readonly>
      <small id="number" class="form-text text-muted">ingresa el numero de id</small>
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Id Padre</label>
      <input type="number" class="form-control" id="exampleInputEmail1" name="padre" value="<?php echo $padre ?>">
      <small id="number" class="form-text text-muted">Introduce el id del padre</small>
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Nombre</label>
      <input type="emailHelp" class="form-control" id="nombre" name="nombre" value="<?php echo $nombre ?>">
      <small id="number" class="form-text text-muted">Asigna un nombre al nuevo contenedor</small>

    </div>
    <div class="row">
      <div class="col-4"></div>
      <button type="submit" class="btn btn-primary col-3">Guardar</button>
    </div>
  </div>
  <div>
  
  <div class="card-footer text-muted text-center">
          Desarrollado por Carlos Rojas
    </div>
  </div>
</form>
</div>
<div class="col-3">  </div>

</div>
</body>
</html>