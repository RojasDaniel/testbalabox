<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen" />

    
</head>
<body class="container">
  <br>
  <div class="row">
<div class="col-2"></div>
<form action="insertar.php" method="POST" class="card col-8  ">
  
  <div class="card-header text-center ">
      <div class="row">
        <p class="col-8"> Inserta un nuevo contenedor</p>
        <div class="col">  <a href="index.php" class="btn btn-success ">Volver al CRUD</a>
      </div>
    </div>
  </div>

  <div class="card-body">
    <div class="form-group">
      <label for="id">Id</label>
      <input type="number" class="form-control" id="id" name="id" placeholder="0">
      <small id="number" class="form-text text-muted">ingresa el numero de id</small>
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Id Padre</label>
      <input type="number" class="form-control" id="exampleInputEmail1" name="padre" placeholder="0">
      <small id="number" class="form-text text-muted">Introduce el id del padre</small>
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Nombre</label>
      <input type="emailHelp" class="form-control" id="nombre" name="nombre">
      <small id="number" class="form-text text-muted">Asigna un nombre al nuevo contenedor</small>

    </div>
    <div class="row">
      <div class="col-5"></div>
        <button type="submit" class="btn btn-primary col-2">Agregar</button>
       
    </div>
    
  </div>
  <div class="card-footer text-muted text-center">
          Desarrollado por Carlos Rojas
    </div>
  </div>
</form>
</div>
</body>
</html>