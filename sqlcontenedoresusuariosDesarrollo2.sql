-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-08-2021 a las 08:41:50
-- Versión del servidor: 10.4.20-MariaDB
-- Versión de PHP: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `contenedoresusuarios`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenedor`
--

CREATE TABLE `contenedor` (
  `idContenedor` int(11) NOT NULL,
  `usuarios_idusuarios` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `contenedor`
--

INSERT INTO `contenedor` (`idContenedor`, `usuarios_idusuarios`, `descripcion`) VALUES
(1, 2, 'X'),
(2, 1, 'YZ'),
(3, 3, 'Z'),
(4, 1, 'B'),
(5, 1, 'R'),
(6, 3, 'G');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contenedores_eliminados`
--

CREATE TABLE `contenedores_eliminados` (
  `idcontenedores_eliminados` int(11) NOT NULL,
  `contenedor_id` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `contenedores_eliminados`
--

INSERT INTO `contenedores_eliminados` (`idcontenedores_eliminados`, `contenedor_id`, `descripcion`) VALUES
(1, 2, 'ok');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuarios`, `nombre`) VALUES
(1, 'Joaquin'),
(2, 'Carlos'),
(3, 'Ana'),
(4, 'Perla');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contenedor`
--
ALTER TABLE `contenedor`
  ADD PRIMARY KEY (`idContenedor`,`usuarios_idusuarios`),
  ADD KEY `usuarios_idusuarios` (`usuarios_idusuarios`);

--
-- Indices de la tabla `contenedores_eliminados`
--
ALTER TABLE `contenedores_eliminados`
  ADD PRIMARY KEY (`idcontenedores_eliminados`,`contenedor_id`),
  ADD KEY `contenedor_id` (`contenedor_id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuarios`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `contenedor`
--
ALTER TABLE `contenedor`
  ADD CONSTRAINT `contenedor_ibfk_1` FOREIGN KEY (`usuarios_idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `contenedores_eliminados`
--
ALTER TABLE `contenedores_eliminados`
  ADD CONSTRAINT `contenedores_eliminados_ibfk_1` FOREIGN KEY (`contenedor_id`) REFERENCES `contenedor` (`idContenedor`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
