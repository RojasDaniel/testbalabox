<?php
$id=$_GET['id'];
$nombre=$_GET['nombre'];
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen" />

</head>
<body class="container">
  <br>
  <div class="row">
    <div class="col-3">  </div>

    <div class="col">  
      <form action="modificarUsuario.php" method="POST" class="card">
      <div class="card-header">
              <div class="row">
                  <p class="col-8"> Actualiza los datos del contenedor eliminado  </p>
                  <a href="index.php" class="btn btn-success col">Volver al CRUD</a>
              </div>
        </div>
        <div class="card-body">
          <div class="form-group">
            <label for="id">Id</label>
            <input type="number" class="form-control" id="id" name="id" value="<?php echo $id ?>" readonly>
            <small id="number" class="form-text text-muted">ingresa el numero de id</small>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Nomre</label>
            <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $nombre ?>" require>
            <small id="text" class="form-text text-muted">Introduce el nombre</small>
          </div>
          <button type="submit" class="btn btn-primary" >Guardar cambios</button>
        </div>
        <div class="card-footer text-muted text-center">
            Desarrollado por Carlos Rojas
        </div>
        
      </form>
    </div>
    <div class="col-3">  </div>

  </div>
</body>
</html>