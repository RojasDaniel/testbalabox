<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
</body>
</html><!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen" />

</head>
<body class="container">
    
  <div class="row">
    <div class="col-3">  </div>
    <div class="col-6">  <br>
      <form action="insertaContenedor.php" method="POST" class="card text-center">
        <div class="card-header ">
          <div class="row">
              <p class="col-8"> Inserta un nuevo contenedor  </p>
              <a href="index.php" class="btn btn-success col">Volver al CRUD</a>
          </div>
        </div>
        <div class="card-body">
          <div class="form-group">
            <label for="id">Id</label>
            <input type="number" class="form-control" id="id" name="id" placeholder="0">
            <small id="number" class="form-text text-muted">Ingresa el numero de id del contenedor</small>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Id usuario</label>
            <input type="number" class="form-control" id="usuario" name="usuario" placeholder="0">
            <small id="number" class="form-text text-muted">Introduce el id del usuario</small>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">descripcion</label>
            <input type="text" class="form-control" id="descripcion" name="descripcion" >
            <small id="text" class="form-text text-muted">Introduce la descripcion</small>
          </div>
          <button type="submit" class="btn btn-primary" >Agregar Contenedor</button>
        </div>
        <div class="card-footer text-muted text-center">
          Desarrollado por Carlos Rojas
        </div>
        </div>
      </form>
    </div>

  </div>
</body>
</html>