<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Desarrollo1</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen" />

</head>
<body class="container"><br>
    <?php
        include 'conexion.php';
        $sql="select idusuarios,nombre from usuarios ORDER BY nombre";
        $resultado = $objetoMysqli->query($sql);
    ?> 
    <div class="card text-center">
    <div class="card-header ">
        <div class="row">
            <p class="col-10"> A continuación, se muestran todos Usuarios</p>
            <a href="index.php" class="btn btn-success col-2">Volver al CRUD</a>
        </div>
    </div>
    <h5 class="card-title">Lista de Usuarios</h5>

        <div class="row card-body">    

            <div class="col-3"></div>    
            <div class="list-group col-6">
                <?php
                while ($filas = $resultado->fetch_assoc()){
                ?>
                <button type="button" class="list-group-item list-group-item-action"aria-current="true" onclick=" location.href='mostrar.php?id=<?php  echo $filas['idusuarios']; ?>&nombre=<?php echo $filas['nombre'] ?>' ">
                <?php echo $filas['nombre'] ?></button>
                <?php
                }
                ?>
            </div>            
         </div>
        <div class="card-footer text-muted">
             Desarrollado por Carlos Rojas
        </div>
    </div>

</body>
</html>