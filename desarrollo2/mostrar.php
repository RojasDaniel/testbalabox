<?php   
    $id=$_GET['id'];
    $nombre=$_GET['nombre'];
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Desarrollo1</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen" />

</head>
<body class="container"><br>
        <?php
            include 'conexion.php';
            $sql="SELECT * FROM usuarios u INNER JOIN contenedor c on u.idusuarios=c.usuarios_idusuarios WHERE u.idusuarios=$id AND  NOT EXISTS (SELECT null from contenedores_eliminados ce WHERE c.idContenedor=ce.contenedor_id);";
            $resultado = $objetoMysqli->query($sql);
        ?>   
    <div class="card text-center">
    <div class="card-header ">
        <div class="row">
            <p class="col-10"> A continuación, se muestran todos los contenedores creados por <?php echo $nombre; ?> </p>
            <a href="inicio.php" class="btn btn-success col-2">Volver a la lista de usuarios</a>
        </div>
    </div>
    
        <div class="row card-body">
            <div class="col-2"></div>    
            <div class="list-group col-8">
                <?php
                while ($filas = $resultado->fetch_assoc()){
                ?>
                    <button type="button" class="list-group-item list-group-item-action"aria-current="true" >
                    <?php echo $filas['descripcion'] ?></button>

                    <?php
                }
                ?>
            </div>
        </div>
        <div class="card-footer text-muted">
            Desarrollado por Carlos Rojas
        </div>
    </div>
</body>
</html>