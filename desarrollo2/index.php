<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen" />

</head>
<body><br>
  <?php
    include 'conexion.php';
  ?>

  <div class="card col">
    <div class="card-header">
        <div class="row">
          <p class="col-sm-10">
            Bienvenido al proyecto 2 de este test, a continuación, se presenta el CRUD para poder realizar el uso respectivo de la base de datos, para continuar a lo requerido, presiona el siguiente botón
          </p> 
          <div class="col-2">
            <button type="button" class="btn btn-primary " onclick=" location.href='inicio.php' ">Iniciar</button>
            <button type="button" class="btn btn-secondary " onclick=" location.href='../index.php' ">Menu</button>
          </div>
        </div>     
    </div>

    <br>
    <div class="row">

    <div class="col-sm-3">
      
      <div class="card">
        <div class="card-body">
          <div class="card-title row">
            <h5 class="col-7" >Usuarios</h5>
            <button type="button" class="btn btn-success col-4" onclick=" location.href='formularioInsertarUsuario.php' ">Agregar</button>
          </div>
          
          
          <table class="table">
              <thead>
                  <tr>
                  <th scope="col">Id</th>
                  <th scope="col">nombre</th>
                  <th scope="col">Acciones</th>

                  </tr>
              </thead>
              <?php
              $sql="select * from usuarios";
              $resultado = $objetoMysqli->query($sql);
              while ($filas = $resultado->fetch_assoc()){

              ?>
              <tbody>
                  <tr>
                  <th scope="row"><?php echo $filas['idusuarios'] ?></th>
                  <td><?php echo $filas['nombre'] ?></td>
                  <td>
                  <a href="eliminarUsuario.php?id=<?php echo $filas['idusuarios'] ?>">Eliminar</a>
                      <a href="formularioModificaUsuario.php?id=<?php echo $filas['idusuarios'] ?>&nombre=<?php echo $filas['nombre']  ?>">Editar</a>
                  </td>
                  </tr>
              </tbody>
              <?php
                }
              ?>
          </table>
      
        </div>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="card">
        <div class="card-body">
          <div class="card-title row">
            <h5 class="col-8" >Contenedores</h5>
            <button type="button" class="btn btn-success col-4" onclick=" location.href='formularioInsertaContenedor.php' ">Agregar</button>
          </div>
          <table class="table">
              <thead>
                  <tr>
                  <th scope="col">id Contenedor</th>
                  <th scope="col">id Creador</th>
                  <th scope="col">Descripcion</th>
                  <th scope="col">Acciones</th>
                  </tr>
              </thead>
              <?php
              $sql="select * from contenedor";
              $resultado = $objetoMysqli->query($sql);
              while ($filas = $resultado->fetch_assoc()){

              ?>
              <tbody>
                  <tr>
                  <th scope="row"><?php echo $filas['idContenedor'] ?></th>
                  <td><?php echo $filas['usuarios_idusuarios'] ?></td>
                  <td><?php echo $filas['descripcion'] ?></td>
                  <td>
                  <a href="eliminarContenedor.php?id=<?php echo $filas['idContenedor'] ?>">Eliminar</a>
                      <a href="formularioModificaContenedor.php?id=<?php echo $filas['idContenedor'] ?>&usuario=<?php echo $filas['usuarios_idusuarios'] ?>&descripcion=<?php echo $filas['descripcion'] ?>">Editar</a>
                  </td>
                  </tr>
              </tbody>
              <?php
                }
              ?>
          </table>


        </div>
      </div>
    </div>
    <div class="col-sm-5">
      <div class="card">
        <div class="card-body">
          <div class="card-title row">
            <h5 class="col-8" >Contenedores eliminados</h5>
            <button type="button" class="btn btn-success col-4" onclick=" location.href='formularioInsertaEliminado.php' ">Agregar</button>
          </div>
          <table class="table">
              <thead>
                  <tr>
                  <th scope="col">id Eliminado</th>
                  <th scope="col">id Contenedor</th>
                  <th scope="col">Descripcion</th>
                  <th scope="col">Acciones</th>
                  </tr>
              </thead>
              <?php
              $sql="select * from contenedores_eliminados";
              $resultado = $objetoMysqli->query($sql);
              while ($filas = $resultado->fetch_assoc()){

              ?>
              <tbody>
                  <tr>
                  <th scope="row"><?php echo $filas['idcontenedores_eliminados'] ?></th>
                  <td><?php echo $filas['contenedor_id'] ?></td>
                  <td><?php echo $filas['descripcion'] ?></td>
                  <td>
                  <a href="eliminarEliminado.php?id=<?php echo $filas['idcontenedores_eliminados'] ?>">Eliminar</a>
                      <a href="formularioModificaEliminado.php?id=<?php echo $filas['idcontenedores_eliminados'] ?>&eliminado=<?php echo $filas['contenedor_id'] ?>&descripcion=<?php  echo $filas['descripcion'] ?>">Editar</a>
                  </td>
                  </tr>
              </tbody>
              <?php
                }
              ?>
          </table>

        </div>
      </div>
    </div></div>
    <br>
    <div class="card-footer text-muted text-center">
          Desarrollado por Carlos Rojas
    </div>
  </div>
  </div>
</body>
</html>