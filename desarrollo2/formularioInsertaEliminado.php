<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen" />

</head>
<body class="container">
<div class="row">
<div class="col-3">  </div>

  <div class="col  text-center">  
    <br>
      <form action="insertaEliminado.php" method="POST" class="card">

         <div class="card-header">
            <div class="row">
                <p class="col-8"> Nueva eliminacion  </p>
                <a href="index.php" class="btn btn-success col">Volver al CRUD</a>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="id">Id</label>
              <input type="number" class="form-control" id="id" name="id" placeholder="0">
              <small id="number" class="form-text text-muted">ingresa el numero de id</small>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Id Eliminado</label>
              <input type="number" class="form-control" id="exampleInputEmail1" name="eliminado" placeholder="0">
              <small id="number" class="form-text text-muted">Introduce el id a marcar como eliminado</small>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Descripcion</label>
              <input type="text" class="form-control" id="exampleInputEmail1" name="descripcion" placeholder="0">
              <small id="text" class="form-text text-muted">Introduce una descripcion de la accion</small>
            </div>
            <button type="submit" class="btn btn-primary" >Agregar marca de eliminado</button>
          </div>
          <div class="card-footer text-muted text-center">
            Desarrollado por Carlos Rojas
          </div>
      </form>
    
  </div>
<div class="col-3">  </div>
</div>
</body>
</html>