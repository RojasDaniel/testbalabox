<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="desarrollo1/css/bootstrap.css" media="screen" />
    
    <title>TestBalabox</title>
</head>
<body class="container">
    <br><br>
    <div class="card text-center">
        <div class="card-header">
            <div class="row">
            <p class="col">
                Bienvenido al test, te invito a verificar cualquiera de los dos Desarrollos planteados, seleccionando alguno de ellos
            </p> 
            </div>     
        </div>
        <div class="card-body">

            <div class="row">        
                <div class="col"> 
                    <a href="desarrollo1/index.php"  class="btn btn-primary rounded-pill" style="height: 300px; width: 500px; font-size:400%"> <br>Desarrollo 1</a>
                    
                </div>
                <div class="col">
                    <a href="desarrollo2/index.php"  class="btn btn-primary rounded-pill" style="height: 300px; width: 500px; font-size:400%"><br>Desarrollo 2</a>
                </div>
            </div>
        </div>
        <div class="card-footer text-muted text-center">
          Desarrollado por Carlos Rojas
        </div>
    </div>

</body>
</html>